﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionOperations
{
    public class Operation
    {
        private static readonly Lazy<Operation> _lazy = new Lazy<Operation>();

        public static Operation Source => _lazy.Value;

        public IEnumerable<T> Intersection<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            return first.Intersect(second).ToList();
        }

        public IEnumerable<T> Union<T>(IEnumerable<T> first, IEnumerable<T> second)
        {
            return first.Union(second).ToList();
        }

        public IEnumerable<T> Concatenate<T>(params IEnumerable<T>[] parameters)
        {
            var concatableCollection = new List<T>();
            foreach (var parameter in parameters)
            {
                concatableCollection.AddRange(parameter);
            }

            return concatableCollection;
        }
    }
}
